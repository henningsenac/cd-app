var pessoa = require('./pessoa.json')
var skills = require('./skills.json')

console.log(pessoa);
module.exports = function (app) {
    app.get('/', function (req, res) {
        res.render('index', { pessoa, skills })
    })
}